function extraRunsPerYear(deliveriesData, matchData) {
  let obj = {};
  for (let macData of matchData) {
    for (let delData of deliveriesData) {
      if (delData.match_id === macData.id && macData.season === "2016") {
        if (obj[delData.bowling_team] === undefined) {
          obj[delData.bowling_team] = parseInt(delData.extra_runs);
        } else {
          obj[delData.bowling_team] += parseInt(delData.extra_runs);
        }
      }
    }
  }
  return obj;
}

module.exports = extraRunsPerYear;
