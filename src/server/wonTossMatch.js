function wonTossMatch(matchData) {
  let obj = {};
  for (let prop of matchData) {
    if (prop.toss_winner === prop.winner) {
      if (obj[prop.winner] === undefined) {
        obj[prop.winner] = 1;
      } else {
        obj[prop.winner] += 1;
      }
    }
  }
  return obj;
}

module.exports = wonTossMatch;
