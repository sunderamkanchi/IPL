function matchYearCount(data) {
  let count = [];
  for (let i = 0; i < data.length; i++) {
    count.push(data[i].season);
  }
  let obj = {};
  for (let i = 0; i < count.length; i++) {
    if (obj.hasOwnProperty(count[i]) === false) {
      obj[count[i]] = 1;
    } else {
      obj[count[i]] += 1;
    }
  }
  return obj;
}
module.exports = matchYearCount;
