let matchesPlayedPerYearResult = "output/testMatchYearCount.json";
let extraRunsPerYear = "output/testExtraRunsPerYear.json";
var Highcharts = require("highcharts");
// Load module after Highcharts is loaded
require("highcharts/modules/exporting")(Highcharts);

fetch(matchesPlayedPerYearResult)
  .then(function (response) {
    return response.json();
  })
  .then(function (data) {
    console.log(data);
    Highcharts.chart("matchesPlayedPerYearResult", {
      chart: {
        type: "column",
      },
      title: {
        text: "Matches Played Per Year",
      },
      subtitle: {
        text: "Source: https://www.kaggle.com/manasgarg/ipl",
      },
      xAxis: {
        categories: Object.keys(data),
        title: {
          text: "Year",
        },
        accessibility: {
          description: "Year",
        },
      },
      yAxis: {
        min: 0,
        tickInterval: 2,
        title: {
          text: "Matches Played",
        },
        labels: {
          overflow: "justify",
          format: "{value}",
        },
      },
      plotOptions: {
        column: {
          dataLabels: {
            enabled: true,
            format: "{y}",
          },
        },
      },
      tooltip: {
        valueSuffix: "",
        stickOnContact: true,
        backgroundColor: "rgba(255, 255, 255, 0.93)",
      },
      legend: {
        enabled: false,
      },
      series: [
        {
          name: "Matches Played",
          data: Object.values(data),
          borderColor: "#5997DE",
        },
      ],
    });
  });

fetch(extraRunsPerYear)
  .then(function (response) {
    return response.json();
  })
  .then(function (data) {
    console.log(data);
    Highcharts.chart("extraRunsPerYear", {
      chart: {
        type: "column",
      },
      title: {
        text: "Extra Runs Per year",
      },
      subtitle: {
        text: "Source: https://www.kaggle.com/manasgarg/ipl",
      },
      xAxis: {
        categories: Object.keys(data),
        title: {
          text: "Year",
        },
        accessibility: {
          description: "Year",
        },
      },
      yAxis: {
        min: 0,
        tickInterval: 2,
        title: {
          text: "Matches Played",
        },
        labels: {
          overflow: "justify",
          format: "{value}",
        },
      },
      plotOptions: {
        column: {
          dataLabels: {
            enabled: true,
            format: "{y}",
          },
        },
      },
      tooltip: {
        valueSuffix: "",
        stickOnContact: true,
        backgroundColor: "rgba(255, 255, 255, 0.93)",
      },
      legend: {
        enabled: false,
      },
      series: [
        {
          name: "Matches Played",
          data: Object.values(data),
          borderColor: "#5997DE",
        },
      ],
    });
  });
