function highestOnePlayerDismissedAnotherplayer(deliveriesData) {
  let dismissalType = [
    "caught",
    "bowled",
    "stumped",
    "caught and bowled",
    "hit wicket",
    "lbw",
  ];
  let obj = {};
  let finalDismissalType = (data) => {
    return dismissalType.includes(data.dismissal_kind);
  };
  deliveriesData
    .filter((delivery) => {
      return finalDismissalType(delivery);
    })
    .forEach((delivery) => {
      let bowler = delivery.bowler;
      let batsman = delivery.player_dismissed;
      let result = `Bowler: ${bowler} Batsman: ${batsman}`;

      if (obj[result] === undefined) {
        obj[result] = 1;
      } else {
        obj[result] += 1;
      }
    });
  //console.log(obj);

  let objToArray = Object.entries(obj);
  objToArray.sort((a, b) => b[1] - a[1]);
  let result = Object.fromEntries([objToArray[0]]);
  //console.log(result);
  return result;
}

module.exports = highestOnePlayerDismissedAnotherplayer;
