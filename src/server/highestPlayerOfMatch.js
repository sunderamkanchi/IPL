function highestPlayerOfMatch(matchData) {
  let obj = {};
  for (let prop of matchData) {
    let year = prop.season;
    let playerName = prop.player_of_match;

    if (obj[year] === undefined) {
      obj[year] = {};
      obj[year][playerName] = 1;
    } else {
      if (obj[year][playerName] === undefined) {
        obj[year][playerName] = 1;
      } else {
        obj[year][playerName] += 1;
      }
    }
  }
  let result = {};
  for (let arr in obj) {
    let arrobj = Object.entries(obj[arr]).sort((a, b) => b[1] - a[1]);

    let finalResult = [arrobj[0]];
    //console.log(finalResult);
    let objarr = Object.fromEntries(finalResult);
    result[arr] = objarr;
  }
  return result;
}

module.exports = highestPlayerOfMatch;
