const csvFilePath = "../data/matches.csv";
const deliveries = "../data/deliveries.csv";
let matchYearCount = require("../server/matchYearCount");
let matchWinnerCount = require("../server/matchWinnerCount");
let extraRunsPerYear = require("../server/extraRunsPerYear");
let topEconomicalBowlers = require("../server/topEconomicalBowlers");
let wonTossMatch = require("../server/wonTossMatch");
let highestPlayerOfMatch = require("../server/highestPlayerOfMatch");
let strikeRateOfBatsman = require("../server/strikeRateOfBatsman");
let bowlerBestEconomySuperOver = require("../server/bowlerBestEconomySuperOver");
let highestOnePlayerDismissedAnotherplayer = require("../server/highestOnePlayerDismissedAnotherPlayer");
const csv = require("csvtojson");
csv()
  .fromFile(csvFilePath)
  .then((matchData) => {
    let matchYearCountresult = matchYearCount(matchData);
    matchYearCountresult = JSON.stringify(matchYearCountresult);
    const fs = require("fs");

    fs.writeFile(
      "../public/output/testMatchYearCount.json",
      matchYearCountresult,
      (err) => {
        if (err) console.log(err);
        else {
          console.log("File written successfully\n");
        }
      }
    );
    let matchWinnerCountResult = matchWinnerCount(matchData);
    matchWinnerCountResult = JSON.stringify(matchWinnerCountResult);
    fs.writeFile(
      "../public/output/testMatchWinnerCount.json",
      matchWinnerCountResult,
      (err) => {
        if (err) console.log(err);
        else {
          console.log("File written successfully\n");
        }
      }
    );
    csv()
      .fromFile(deliveries)
      .then((deliveriesData) => {
        let extraRunsPerYearResult = extraRunsPerYear(
          deliveriesData,
          matchData
        );
        extraRunsPerYearResult = JSON.stringify(extraRunsPerYearResult);
        fs.writeFile(
          "../public/output/testExtraRunsPerYear.json",
          extraRunsPerYearResult,
          (err) => {
            if (err) console.log(err);
            else {
              console.log("File written successfully\n");
            }
          }
        );
        let topEconomicalBowlersResult = topEconomicalBowlers(
          deliveriesData,
          matchData
        );
        //console.log(topEconomicalBowlersResult);
        topEconomicalBowlersResult = JSON.stringify(topEconomicalBowlersResult);
        fs.writeFile(
          "../public/output/testTopEconomicalBowlers.json",
          topEconomicalBowlersResult,
          (err) => {
            if (err) console.log(err);
            else {
              console.log("File written successfully\n");
            }
          }
        );
        wonTossMatchResult = wonTossMatch(matchData);
        wonTossMatchResult = JSON.stringify(wonTossMatchResult);
        fs.writeFile(
          "../public/output/testWonTossMatch.json",
          wonTossMatchResult,
          (err) => {
            if (err) console.log(err);
            else {
              console.log("File written successfully\n");
            }
          }
        );
        let highestPlayerOfMatchResult = highestPlayerOfMatch(matchData);
        highestPlayerOfMatchResult = JSON.stringify(highestPlayerOfMatchResult);
        fs.writeFile(
          "../public/output/testHighestPlayerOfMatch.json",
          highestPlayerOfMatchResult,
          (err) => {
            if (err) console.log(err);
            else {
              console.log("File written successfully\n");
            }
          }
        );
        let strikeRateOfBatsmanResult = strikeRateOfBatsman(
          deliveriesData,
          matchData
        );
        strikeRateOfBatsmanResult = JSON.stringify(strikeRateOfBatsmanResult);
        fs.writeFile(
          "../public/output/testStrikeRateOfBatsman.json",
          strikeRateOfBatsmanResult,
          (err) => {
            if (err) console.log(err);
            else {
              console.log("File written successfully\n");
            }
          }
        );
        let bowlerBestEconomySuperOverResult =
          bowlerBestEconomySuperOver(deliveriesData);
        bowlerBestEconomySuperOverResult = JSON.stringify(
          bowlerBestEconomySuperOverResult
        );
        fs.writeFile(
          "../public/output/bowlerBestEconomySuperOver.json",
          bowlerBestEconomySuperOverResult,
          (err) => {
            if (err) console.log(err);
            else {
              console.log("File written successfully\n");
            }
          }
        );
        let highestOnePlayerDismissedAnotherPlayerResult =
          highestOnePlayerDismissedAnotherplayer(deliveriesData);
        highestOnePlayerDismissedAnotherPlayerResult = JSON.stringify(
          highestOnePlayerDismissedAnotherPlayerResult
        );
        fs.writeFile(
          "../public/output/highestOnePlayerDismissedAnotherPlayer.json",
          highestOnePlayerDismissedAnotherPlayerResult,
          (err) => {
            if (err) console.log(err);
            else {
              console.log("File written successfully\n");
            }
          }
        );
      });
  });
