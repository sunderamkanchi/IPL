function fetchMatchesPlayedPerYear() {
  fetch("output/testMatchYearCount.json")
    .then((result) => result.json())
    .then(visualizeMatchesPlayedPerYear);
}

function fetchMatchesOwnPerTeamPerYear() {
  fetch("output/testMatchWinnerCount.json")
    .then((result) => result.json())
    .then(visualizeMatchesOwnPerTeamPerYear);
}

function fetchExtraRuns2k16() {
  fetch("output/testExtraRunsPerYear.json")
    .then((result) => result.json())
    .then(visualizeExtraRuns2k16);
}
function fetchEconomicalBowlers2k15() {
  fetch("output/testTopEconomicalBowlers.json")
    .then((result) => result.json())
    .then(visualizeEconomicalBowlers2k15);
}

function visualizeMatchesPlayedPerYear(data) {
  Highcharts.chart("chart", {
    chart: {
      type: "column",
    },
    title: {
      text: "Matches Played Per Season",
    },
    xAxis: {
      type: "category",
      title: {
        text: "Season",
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: "Matches Played",
      },
    },
    legend: {
      enabled: false,
    },

    series: [
      {
        data: Object.entries(data),
        colorByPoint: true,
        dataLabels: {
          enabled: true,
          color: "#FFFFFF",
          align: "center",
          y: 30, // 30 pixels down from the top
        },
      },
    ],
  });
}

function visualizeMatchesOwnPerTeamPerYear(data) {
  const season = Object.keys(data);
  data = Object.values(data);
  data = data.reduce((acc, obj) => {
    const keys = Object.keys(obj);
    keys.forEach((key) => {
      if (acc[key] !== undefined) {
        acc[key].push(obj[key]);
      } else {
        acc[key] = [];
      }
    });
    return acc;
  }, {});

  data = Object.entries(data);

  data = data.map((arr) => {
    return { name: arr[0], data: arr[1] };
  });

  Highcharts.chart("chart", {
    chart: {
      type: "bar",
    },
    title: {
      text: "Matches Own Per Team Per Year",
    },
    xAxis: {
      categories: season,
      min: 0,
      title: {
        text: "Season",
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: "Matches Own Count",
      },
    },
    legend: {
      reversed: true,
    },
    plotOptions: {
      series: {
        stacking: "normal",
      },
    },
    series: data,
  });
}

function visualizeExtraRuns2k16(data) {
  Highcharts.chart("chart", {
    chart: {
      type: "column",
    },
    title: {
      text: "Extra Runs conceded in 2016 by all the teams",
    },
    xAxis: {
      type: "category",
      title: {
        text: "Teams",
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: "Extra Runs",
      },
    },
    legend: {
      enabled: false,
    },

    series: [
      {
        name: "Population",
        data: Object.entries(data),
        colorByPoint: true,
        dataLabels: {
          enabled: true,
          color: "#FFFFFF",
          align: "center",
          y: 30, // 10 pixels down from the top
        },
      },
    ],
  });
}
function visualizeEconomicalBowlers2k15(data) {
  let result = data.reduce((acc, data) => {
    dataArr = Object.entries(data);
    let name = dataArr[0][1];
    let economy = dataArr[3][1];
    if (!(name in acc)) {
      acc[name] = economy;
    }
    return acc;
  }, {});
  console.log(result);

  Highcharts.chart("chart", {
    chart: {
      type: "column",
    },
    title: {
      text: "Top 10 Economical Bowlers in 2016",
    },
    xAxis: {
      type: "category",
      title: {
        text: "Bowlers",
      },
    },
    yAxis: {
      min: 0,
      title: {
        text: "Economical Rate",
      },
    },
    legend: {
      enabled: false,
    },

    series: [
      {
        name: "Population",
        data: Object.entries(result),
        colorByPoint: true,
        dataLabels: {
          enabled: true,
          color: "#FFFFFF",
          align: "center",
          format: "{point.y:.1f}", // one decimal
          y: 30, // 30 pixels down from the top
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
    ],
  });
}
